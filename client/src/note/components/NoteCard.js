import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import LinearProgress from '@material-ui/core/LinearProgress'

import IconButton from '@material-ui/core/IconButton'
import HighlightOffTwoToneIcon from '@material-ui/icons/HighlightOffTwoTone'


const useStyles = makeStyles((theme) => ({
  detailsContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%'
  },
  cardContent: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'start',
    borderTop: '1px solid #d6d6d6'
  }
}))


const NoteDetails = props => {
  const classes = useStyles()
  const { content } = props
  return (
    <div className={classes.detailsContainer}>
      <Typography>
        {content}
      </Typography>
    </div>
  )
}

const NoteCard = props => {
  const classes = useStyles()
  const { data } = props
  const { name, content } = data

  const loaderVariant = props.processing ? 'indeterminate' : 'determinate'

  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card className={classes.card} variant='outlined'>
        <CardHeader
          action={
            <IconButton aria-label='settings' onClick={props.onDelete}>
              <HighlightOffTwoToneIcon />
            </IconButton>
          }
          title={(
            <Typography variant='h5' display='inline'>
              {name}
            </Typography>
          )}
        />
        <LinearProgress variant={loaderVariant} value={0}/>
        <CardContent className={classes.cardContent}>
            <NoteDetails content={content} />
        </CardContent>
      </Card>
    </Grid>
  )
}
export default NoteCard