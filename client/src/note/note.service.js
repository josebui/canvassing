const SERVICE_URL = 'http://localhost:5000/api' 


export const loadNotes = () => fetch(
  `${SERVICE_URL}/note/`,
  {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  }
)
  .then(res => res.json())
  .then(json => (!json)
    ? Promise.reject(json.message)
    : json
  )

export const addNote = note => fetch(
  `${SERVICE_URL}/note/create`,
  {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(note)
  }
)
  .then(res => res.json())
  .then(json => (!json.note)
    ? Promise.reject(json.message)
    : json.note
  )

export const removeNote = id => fetch(
  `${SERVICE_URL}/note/${id}`,
  {
    method: 'DELETE'
  }
)
  .then(res => res.json())