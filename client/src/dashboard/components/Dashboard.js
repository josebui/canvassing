import React, { useRef, useEffect } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import Grid from '@material-ui/core/Grid'
import TextField from '@material-ui/core/TextField'
import Container from '@material-ui/core/Container'
import { Typography, Fab, CircularProgress } from '@material-ui/core'

import AddTwoToneIcon from '@material-ui/icons/AddTwoTone'

import { loadNotes, addNote, removeNote } from '../dashboard.actions'

import NoteCard from '../../note/components/NoteCard'

const useStyles = makeStyles((theme) => ({
  noteFormContainer: {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  noteFormInputsContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  noteFormInputsContent: {
    marginTop: theme.spacing(1)
  },

  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 2),
  },
  cardGrid: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  wrapper: {
    marginLeft: theme.spacing(2),
    margin: theme.spacing(1),
    position: 'relative',
  },
  fabProgress: {
    position: 'absolute',
    top: -6,
    left: -6,
    zIndex: 1,
  }
}))

const NoteForm = props => {
  const classes = useStyles()
  const nameInput = useRef(null)
  const contentInput = useRef(null)

  const onAdd = () => {
    props.addNote({
      name: nameInput.current.value,
      content: contentInput.current.value
    })
  }

  useEffect(() => {
    if (!props.notes) {
      props.loadNotes()
    }
    return () => {}
  })

  return (
    <>
      <div className={classes.noteFormContainer}>
        <div className={classes.noteFormInputsContainer}>
          <TextField inputRef={nameInput} label='Name' variant='outlined' />
          <TextField
            label="Notes"
            inputRef={contentInput}
            className={classes.noteFormInputsContent}
            multiline
            rows={4}
            variant="outlined"
          />
        </div>
        <div className={classes.wrapper}>
          <Fab
            className={classes.noteFormButton}
            aria-label='Add Note'
            color='primary'
            disabled={props.processing}
            onClick={() => onAdd()}
          >
              <AddTwoToneIcon />
          </Fab>
          {props.processing && <CircularProgress color='secondary' size={68} className={classes.fabProgress} />}
        </div>
      </div>
      { !props.error || 
        <Typography variant='caption' display='block' color='error' align='center'>{props.error}</Typography>
      }
    </>
  )
}

const Dashboard = props => {
  const classes = useStyles()

  return (
      <>
        <Container className={classes.cardGrid} maxWidth='md'>
          <Grid container spacing={1}>
            {<Grid key={-1} item xs={12} sm={6} md={4}>
              <NoteForm {...props} />
            </Grid>}
            {!props.notes ? <></> : props.notes.map(note => (
              <NoteCard
                key={note.id}
                data={note}
                processing={note.processing}
                onDelete={() => props.removeNote(note.id)}
              />
            ))}
          </Grid>
        </Container>
      </>
  )
}

export default connect(
  state => ({
    processing: state.dashboard.processing,
    processingByPlace: state.dashboard.processingByPlace,
    error: state.dashboard.error,
    notes: state.dashboard.notes
  }),
  dispatch => ({
    loadNotes: () => dispatch(loadNotes()),
    addNote: note => dispatch(addNote(note)),
    removeNote: id => dispatch(removeNote(id))
  })
)(Dashboard)
