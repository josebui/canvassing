import {
  ADD_NOTE_START,
  ADD_NOTE_ERROR,
  ADD_NOTE_SUCCESS,
  LOAD_NOTES_SUCCESS,
  REMOVE_NOTE_START,
  REMOVE_NOTE_ERROR,
  REMOVE_NOTE_SUCCESS
} from './dashboard.actions'


const initialState = {
  notes: null,
  processing: false,
  processingByPlace: {},
  error: null
}

const reducer = (state = initialState, action) => {
  const reducers = {
    [LOAD_NOTES_SUCCESS]: () => ({
      ...state,
      notes: action.payload.notes
    }),
    [ADD_NOTE_START]: () => ({
      ...state,
      processing: true,
      error: null
    }),
    [ADD_NOTE_ERROR]: () => ({
      ...state,
      processing: false,
      error: action.payload.error
    }),
    [ADD_NOTE_SUCCESS]: () => ({
      ...state,
      processing: false,
      error: null,
      notes: [
        ...state.notes,
        action.payload.data
      ]
    }),
    [REMOVE_NOTE_START]: () => ({
      ...state,
      notes: state.notes
        .map(note => (action.payload.id !== note.id)
          ? note
          : {
            ...note,
            processing: true
          }
        )
    }),
    [REMOVE_NOTE_ERROR]: () => ({
      ...state,
      notes: state.notes
        .map(note => (action.payload.id !== note.id)
          ? note
          : {
            ...note,
            processing: false
          }
        )
    }),
    [REMOVE_NOTE_SUCCESS]: () => {
      return ({
        ...state,
        notes: state.notes
          .map(note => (action.payload.id !== note.id)
            ? note
            : null
          )
          .filter(note => !!note)
      })
    }
  }
  return reducers[action.type]?.() || state
}

export default reducer