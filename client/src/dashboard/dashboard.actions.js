import * as noteService from '../note/note.service'

export const LOAD_NOTES_START = 'LOAD_NOTES_START'
export const LOAD_NOTES_ERROR = 'LOAD_NOTES_ERROR'
export const LOAD_NOTES_SUCCESS = 'LOAD_NOTES_SUCCESS'

export const ADD_NOTE_START = 'ADD_NOTE_START'
export const ADD_NOTE_ERROR = 'ADD_NOTE_ERROR'
export const ADD_NOTE_SUCCESS = 'ADD_NOTE_SUCCESS'

export const REMOVE_NOTE_START = 'REMOVE_NOTE_START'
export const REMOVE_NOTE_ERROR = 'REMOVE_NOTE_ERROR'
export const REMOVE_NOTE_SUCCESS = 'REMOVE_NOTE_SUCCESS'

export const loadNotesStart = () => ({
  type: LOAD_NOTES_START
})
export const loadNotesError = () => ({
  type: LOAD_NOTES_ERROR
})
export const loadNotesSuccess = data => ({
  type: LOAD_NOTES_SUCCESS,
  payload: data
})

export const addNoteStart = () => ({
  type: ADD_NOTE_START
})
export const addNoteError = error => ({
  type: ADD_NOTE_ERROR,
  payload: { error }
})
export const addNoteSuccess = data => ({
  type: ADD_NOTE_SUCCESS,
  payload: { data }
})

export const removeNoteStart = id => ({
  type: REMOVE_NOTE_START,
  payload: { id }
})
export const removeNoteError = (id, error) => ({
  type: REMOVE_NOTE_ERROR,
  payload: { id, error }
})
export const removeNoteSuccess = id => ({
  type: REMOVE_NOTE_SUCCESS, 
  payload: { id }
})

export const loadNotes = () => dispatch => {
  dispatch(loadNotesStart())
  noteService.loadNotes()
    .then(response => {
      dispatch(loadNotesSuccess({
        ...response
      }))
    })
    .catch(error => {
      dispatch(loadNotesError(error))
    })
}

export const addNote = ({ name, content }) => dispatch => {
  dispatch(addNoteStart())
  noteService.addNote({ name, content })
    .then(response => {
      dispatch(addNoteSuccess({
        ...response
      }))
    })
    .catch(error => {
      dispatch(addNoteError(error))
    })
}

export const removeNote = (id) => dispatch => {
  dispatch(removeNoteStart(id))
  noteService.removeNote(id)
    .then(response => {
      dispatch(removeNoteSuccess(id))
    })
    .catch(error => {
      dispatch(removeNoteError(id, error))
    })
}