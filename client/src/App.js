import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import AppBar from '@material-ui/core/AppBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'

import NoteTwoToneIcon from '@material-ui/icons/NoteTwoTone'

import Dashboard from './dashboard/components/Dashboard'


const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  }
}))

function App() {
  const classes = useStyles()
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="relative">
        <Toolbar>
          <NoteTwoToneIcon className={classes.icon} />
          <Typography variant="h6" color="inherit" noWrap>
              Canvassing
          </Typography>
        </Toolbar>
      </AppBar>
      <main>
        <Dashboard />
      </main>
    </React.Fragment>
  )
}

export default App;
