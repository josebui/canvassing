import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import dashboard from './dashboard/dashboard.reducer.js'

const rootReducer = combineReducers({ dashboard })


const store = createStore(
  rootReducer,
  {},
  applyMiddleware(thunk)
)

export default store