#!/bin/bash

CONTAINER_NAME="empower-db"
DB_NAME="empower"
DB_USER="empower"
DB_PASSWORD="abc123"
DB_IMAGE="postgres"
LOCAL_PORT="5432"

function setEnv() {
  if [[ -z "${NODE_ENV}" ]]; then
    NODE_END="dev"
  fi

  if [ "$NODE_ENV" == "production" ]; then
    WORK_DIR="build"
  else
    WORK_DIR="src"
  fi
}

function start() {
  docker start $DB_CONTAINER
}

function create() {
  docker run --name "$DB_CONTAINER" \
    -e POSTGRES_PASSWORD=$DB_PASSWORD \
    -e POSTGRES_USER=$DB_USER \
    -e POSTGRES_DB=$DB_NAME \
    -p "$LOCAL_PORT:5432" \
    -d $DB_IMAGE
}

function runQuery() {
  container_exist="$(docker ps -a -q -f name=$DB_CONTAINER)"
  if [ ! $container_exist ]; then
    echo "You need to run start first"
  else
    # Container running, run command inside container
    docker exec -it \
      -e PGPASSWORD=$DB_PASSWORD \
      -e PGUSER=$DB_USER \
      "$DB_CONTAINER" bash -c "$1"
  fi
}

function destroy () {
  docker stop $DB_CONTAINER
  docker rm $DB_CONTAINER
}

function connect() {
  runQuery "psql $DB_NAME $DB_USER"
}

function migrateUp() {
  setEnv
  node_modules/.bin/sequelize db:migrate --config ./$WORK_DIR/config/db.js
}

function migrateDown() {
  setEnv
  node_modules/.bin/sequelize db:migrate:undo --config ./$WORK_DIR/config/db.js
}

if [ -z "$1" ]; then
  echo "Function name is required";
fi

$1
