module.exports = {
  "presets": ["@babel/env"],
  "sourceMaps": "inline",
  "retainLines": true,
  "plugins": [
    [
      "babel-plugin-root-import",
      {
        "rootPathSuffix": "src"
      }
    ],
    "@babel/plugin-transform-spread"
  ]
}
