# Canvassing application



## Run server

1. `yarn`
2. `yarn start`

## Run client

1. `cd client`
2. `yarn`
3. `yarn start`

## Build client

1. `cd client`
2. `yarn build`

## Run DB

1. `yarn run db-create`

## Run DB migrations

1. `yarn run db-migrations-up`


## Potential improvements
- Handle intial loader for notes
- Add unit tests for client and server
- Handle configuration in client code
- Improve UI
- Authentication
- Notes by user
- Deploy to a server