import express from 'express'
import cors from 'cors'

import * as db from '~/db'
import apiRouter from '~/api.routes'

const PORT = process.env.PORT || 5000

const app = express()

app.use(cors())
app.use(express.json())

// Routes
app.use('/api', apiRouter)

// Client
app.use('/', express.static('client/build'))


// DB
db.sequelize
  .authenticate()
  .then(() => app.listen(PORT, () => console.log(`Listening on ${ PORT }`)))
  .catch(err =>
    console.log('Server failed to start due to error: %s', err)
  )