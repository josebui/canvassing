import { Sequelize } from 'sequelize'

import config from '~/config'

const { db, dbOptions } = config

export const sequelize = new Sequelize(
	db.database,
	db.username,
	db.password, {
		host: db.host,
		dialect: db.dialect,
		...dbOptions
	}
)

// Models
// export const Note = sequelize['import']('note/note.model')
// export const Note = require('note/note.model.js')(sequelize, Sequelize.DataTypes)

