import { DataTypes } from 'sequelize'

import { sequelize } from '~/db'

export default sequelize.define('Note', {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4
  },
  name: DataTypes.STRING,
  content: DataTypes.STRING,
  deleted: DataTypes.BOOLEAN,
  createdAt: DataTypes.DATE,
  updatedAt: DataTypes.DATE,
  version: {
    type: DataTypes.BIGINT,
    defaultValue: 0
  }
}, {
  version: true
})
