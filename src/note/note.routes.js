import express from 'express'

import * as controller from '~/note/note.controller'


const router = express.Router()
router.post('/create', controller.create)
router.get('/', controller.findAll)
router.delete('/:id', controller.remove)

export default router
