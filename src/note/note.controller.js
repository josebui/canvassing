import _ from 'lodash'

import Note from '~/note/note.model'


const isDbError = error => {
  return _.includes([
    'ValidationError',
    'UniqueConstraintError'
  ], error.constructor.name)
}

const getDbError = error => error.errors.map(err => ({
  message: err.message,
  path: err.path
}))

export const validationError = (res, statusCode) => {
  statusCode = statusCode || 404
  return err => {
    const response = isDbError(err)
      ? getDbError(err)
      : err.message
      ? { message: err.message }
      : { message: 'Unknow error' }
    res.status(statusCode).json(response)
  }
}

export const create = (req, res, next) => {
  return Note.create({
    ...req.body,
    deleted: false
  })
    .then(note => {
      res.json({ note })
    })
    .catch(error => validationError(res)(error))
}

export const remove = (req, res, next) => {
  return Note.update({ deleted: true }, {
    where: {
      id: req.params.id
    }
  })
    .then(() => {
      res.json({ result: true })
    })
    .catch(error => validationError(res)(error))
}

export const findAll = (req, res, next) => {
  return Note.findAll({
    where: {
      deleted: false
    }
  })
    .then(notes => {
      res.json({ notes })
    })
    .catch(error => validationError(res)(error))
}