import _ from 'lodash'

import db from '~/config/db'

const env = process.env.NODE_ENV || 'development'

const dbConfig = db[env]

const defaults = {
  environment: env,
  db: dbConfig,
  dbOptions: {
    logging: false,
    define: {
      timestamps: true
    }
  }
}

const config = _.merge(
  defaults,
  require(`./${env}.js`).default || {}
)

export default config
