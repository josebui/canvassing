'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'Notes',
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.UUIDV4
        },
        name: Sequelize.STRING,
        content: Sequelize.STRING,
        deleted: Sequelize.BOOLEAN,
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE,
        version: Sequelize.BIGINT
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Notes')
  }
};
